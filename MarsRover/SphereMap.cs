﻿namespace MarsRover
{
    public class SphereMap : IMap
    {
        public int LengthX { get; private set; }

        public int LengthY { get; private set; }

        public SphereMap(int lengthX, int lengthY)
        {
            LengthX = lengthX;
            LengthY = lengthY;
        }

        public int MovePositionX(int tryX)
        {
            return GetInRange(tryX, LengthX);
        }

        public int MovePositionY(int tryY)
        {
            return GetInRange(tryY, LengthY);
        }

        public bool IsTopPoint(int y)
        {
            return y == LengthY - 1;
        }

        private int GetInRange(int value, int length)
        {
            if (value < 0) value = length - 1;
            else if (value >= length) value = 0;

            return value;
        }
    }
}
