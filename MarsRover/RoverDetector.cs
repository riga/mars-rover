﻿using System;

namespace MarsRover
{
    public class RoverDetector : IRover
    {
        private readonly IRover _rover;
        private readonly IObstacleDetector _detector;

        public int X
        {
            get { return _rover.X; }
        }

        public int Y
        {
            get { return _rover.Y; }
        }

        public Orientation Orientation
        {
            get { return _rover.Orientation; }
        }

        public IMap Map
        {
            get { return _rover.Map; }
        }

        public RoverDetector(IRover rover, IObstacleDetector detector)
        {
            if (rover == null) throw new ArgumentException("Rover should not be null.");
            if (detector == null) throw new ArgumentException("Detector should not be null.");

            _rover = rover;
            _detector = detector;
        }

        public bool MoveForward()
        {
            return Move(1);
        }

        public bool MoveBackward()
        {
            return Move(-1);
        }

        private bool Move(int step)
        {
            var x = X;
            var y = Y;
            switch (Orientation)
            {
                case Orientation.West:
                    x = _rover.Map.MovePositionX(x - step);
                    break;
                case Orientation.East:
                    x = _rover.Map.MovePositionX(X + step);
                    break;
                case Orientation.North:
                    y = Map.MovePositionY(Y + step);
                    break;
                case Orientation.South:
                    y = Map.MovePositionY(Y - step);
                    break;
            }

            if (!_detector.IsThereObstacle(x, y))
            {
                _rover.MoveForward();
                return false;
            }

            MoveTopPossible();
            return true;
        }

        public void TurnLeft()
        {
            _rover.TurnLeft();
        }

        public void TurnRight()
        {
            _rover.TurnRight();
        }

        public void MoveTopPossible()
        {
            TurnNorth();
            MoveTop();
        }

        private void TurnNorth()
        {
            if (_rover.Orientation == Orientation.South)
            {
                _rover.TurnLeft();
                _rover.TurnLeft();
            }
            else if (_rover.Orientation == Orientation.West)
            {
                _rover.TurnRight();
            }
            else if (_rover.Orientation == Orientation.East)
            {
                _rover.TurnLeft();
            }
        }

        private void MoveTop()
        {
            while(!_rover.Map.IsTopPoint(_rover.Y) && !_detector.IsThereObstacle(_rover.X, _rover.Y + 1))
            {
                _rover.MoveForward();
            }            
        }
    }
}