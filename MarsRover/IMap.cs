﻿namespace MarsRover
{
    public interface IMap
    {
        int LengthX { get; }

        int LengthY { get; }

        int MovePositionX(int tryX);

        int MovePositionY(int tryY);

        bool IsTopPoint(int y);
    }
}
