﻿namespace MarsRover
{
    public interface IRover
    {
        int X { get; }

        int Y { get; }

        Orientation Orientation { get; }

        IMap Map { get; }

        bool MoveForward();

        bool MoveBackward();

        void TurnLeft();

        void TurnRight();
    }
}
