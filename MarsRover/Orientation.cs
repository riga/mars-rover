﻿namespace MarsRover
{
    public enum Orientation
    {
        North,
        South,
        East,
        West
    }
}
