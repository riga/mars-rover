﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MarsRover
{
    /// <summary>
    /// Just a mock or the real detector!
    /// </summary>
    public class SimpleDetector : IObstacleDetector
    {
        private readonly List<Tuple<int, int>> _obstacles;

        public SimpleDetector(List<Tuple<int, int>> obstacles)
        {
            if (obstacles == null) throw new ArgumentException("Obstacles should not be null.");
         
            _obstacles = obstacles;
        }

        public bool IsThereObstacle(int x, int y)
        {
            return _obstacles.Any(obstacle => obstacle.Item1 == x && obstacle.Item2 == y);
        }
    }
}
