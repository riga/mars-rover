﻿using System;

namespace MarsRover
{
    public class Rover : IRover
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public Orientation Orientation { get; private set; }
        public IMap Map { get; private set; }

        public Rover(int x, int y, Orientation orientation, IMap map)
        {
            if (map == null) throw new ArgumentException("Map cannot be null.");

            X = x;
            Y = y;
            Orientation = orientation;
            Map = map;
        }

        public bool MoveForward()
        {
            return Move(1);
        }

        public bool MoveBackward()
        {
            return Move(-1);
        }

        private bool Move(int step)
        {
            switch (Orientation)
            {
                case Orientation.West:
                    X = Map.MovePositionX(X - step);
                    break;
                case Orientation.East:
                    X = Map.MovePositionX(X + step);
                    break;
                case Orientation.North:
                    Y = Map.MovePositionY(Y + step);
                    break;
                case Orientation.South:
                    Y = Map.MovePositionY(Y - step);
                    break;
            }

            return false;
        }

        public void TurnLeft()
        {
            switch (Orientation)
            {
                case Orientation.West:
                    Orientation = Orientation.South;
                    break;
                case Orientation.East:
                    Orientation = Orientation.North;
                    break;
                case Orientation.North:
                    Orientation = Orientation.West;
                    break;
                case Orientation.South:
                    Orientation = Orientation.East;
                    break;
            }
        }

        public void TurnRight()
        {
            switch (Orientation)
            {
                case Orientation.West:
                    Orientation = Orientation.North;
                    break;
                case Orientation.East:
                    Orientation = Orientation.South;
                    break;
                case Orientation.North:
                    Orientation = Orientation.East;
                    break;
                case Orientation.South:
                    Orientation = Orientation.West;
                    break;
            }
        }
    }
}
