﻿using System;

namespace MarsRover
{
    public class Control
    {
        private readonly IRover _rover;

        public Control(IRover rover)
        {
            if (rover == null) throw new ArgumentException("Rover can not be null.");

            _rover = rover;
        }

        public void SendCommands(string commands)
        {
            foreach (var command in commands)
            {
                switch (command)
                {
                    case 'F':
                        _rover.MoveForward();
                        break;
                    case 'B':
                        _rover.MoveBackward();
                        break;
                    case 'L':
                        _rover.TurnLeft();
                        break;
                    case 'R':
                        _rover.TurnRight();
                        break;
                }
            }
        }
    }
}
