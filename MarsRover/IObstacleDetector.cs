﻿namespace MarsRover
{
    public interface IObstacleDetector
    {
        bool IsThereObstacle(int x, int y);
    }
}
