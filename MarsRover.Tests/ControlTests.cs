﻿using System;
using NUnit.Framework;

namespace MarsRover.Tests
{
    [TestFixture]
    public class ControlTests
    {
        private RoverStub _rover;
        private Control _control;

        [SetUp]
        public void SetUp()
        {
            _rover = new RoverStub();
            _control = new Control(_rover);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Ctor_RoverNull_ThrowException()
        {
            _control = new Control(null);
        }

        [Test]
        [ExpectedException(typeof (NullReferenceException))]
        public void SendCommands_Null_ThrowException()
        {
            _control.SendCommands(null);
        }

        [TestCase("" , 0, 0, 0, 0)]
        [TestCase("F", 1, 0, 0, 0)]
        [TestCase("B", 0, 1, 0, 0)]
        [TestCase("L", 0, 0, 1, 0)]
        [TestCase("R", 0, 0, 0, 1)]
        [TestCase("X", 0, 0, 0, 0)]
        [TestCase("FBLR", 1, 1, 1, 1)]
        public void SendCommands_Commands_RoverMovesForwardBackwardTurnLeftRight(string commands, int forwardCount, int backwardCount, int leftCount, int rightCount)
        {
            _control.SendCommands(commands);

            Assert.AreEqual(forwardCount, _rover.MoveForwardCount);
            Assert.AreEqual(backwardCount, _rover.MoveBackwardCount);
            Assert.AreEqual(leftCount, _rover.TurnLeftCount);
            Assert.AreEqual(rightCount, _rover.TurnRightCount);
        }
    }

    public class RoverStub : IRover
    {
        public int MoveForwardCount = 0;
        public int MoveBackwardCount = 0;
        public int TurnLeftCount = 0;
        public int TurnRightCount = 0;

        public int X { get; private set; }
        public int Y { get; private set; }
        public Orientation Orientation { get; private set; }
        public IMap Map { get; private set; }

        public bool MoveForward()
        {
            MoveForwardCount++;
            return false;
        }

        public bool MoveBackward()
        {
            MoveBackwardCount++;
            return false;
        }

        public void TurnLeft()
        {
            TurnLeftCount++;
        }

        public void TurnRight()
        {
            TurnRightCount++;
        }
    }
}
