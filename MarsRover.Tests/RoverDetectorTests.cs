﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace MarsRover.Tests
{
    [TestFixture]
    public class RoverDetectorTests
    {
        private SphereMap _map;
        private SimpleDetector _detector;
        private const int LengthX = 100;
        private const int LengthY = 50;

        [SetUp]
        public void SetUp()
        {
            _map = new SphereMap(LengthX, LengthY);

            var obstacles = new List<Tuple<int, int>>
                         {
                             //Goes to top most
                             new Tuple<int, int>(1, 0),
                             //Stops before top because hits another obstacle
                             new Tuple<int, int>(4, 0), new Tuple<int, int>(3, 10), new Tuple<int, int>(5, 10),
                         };

            _detector = new SimpleDetector(obstacles);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Ctor_RoverNull_ThrowsException()
        {
            var roverDetector = new RoverDetector(null, _detector);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Ctor_ObstaclesNull_ThrowsException()
        {
            var rover = new Rover(0, 0, Orientation.North, _map);
            var roverDetector = new RoverDetector(rover, null);
        }

        #region -------- Test it behaves as before when no obstacles ---------
        [TestCase(0, 0, Orientation.West, LengthX - 1, 0)]
        [TestCase(LengthX - 1, 0, Orientation.West, LengthX - 2, 0)]
        [TestCase(0, 0, Orientation.East, 1, 0)]
        [TestCase(LengthX - 1, 0, Orientation.East, 0, 0)]
        [TestCase(0, 0, Orientation.North, 0, 1)]
        [TestCase(0, LengthY - 1, Orientation.North, 0, 0)]
        [TestCase(0, 0, Orientation.South, 0, LengthY - 1)]
        [TestCase(0, LengthY - 1, Orientation.South, 0, LengthY - 2)]
        public void MoveForward_CasePositionXandYOrientation_ChangesPositionXorYDependingOnOrientation(int x, int y, Orientation orientation, int outX, int outY)
        {
            var rover = new Rover(x, y, orientation, _map);
            var roverDetector = new RoverDetector(rover, new SimpleDetector(new List<Tuple<int, int>> {}));
            var wasObstacle= roverDetector.MoveForward();

            Assert.AreEqual(outX, roverDetector.X);
            Assert.AreEqual(outY, roverDetector.Y);
            Assert.AreEqual(orientation, roverDetector.Orientation);
            Assert.AreEqual(false, wasObstacle);
        }

        [TestCase(LengthX - 1, 0, Orientation.West, LengthX - 2, 0)]
        [TestCase(0, 0, Orientation.West, LengthX - 1, 0)]
        [TestCase(LengthX - 1, 0, Orientation.East, 0, 0)]
        [TestCase(0, 0, Orientation.East, 1, 0)]
        [TestCase(0, LengthY - 1, Orientation.North, 0, 0)]
        [TestCase(0, 0, Orientation.North, 0, 1)]
        [TestCase(0, LengthY - 1, Orientation.South, 0, LengthY - 2)]
        [TestCase(0, 0, Orientation.South, 0, LengthY - 1)]
        public void MoveBackward_CasePositionXandYOrientation_ChangesPositionXorYDependingOnOrientation(int x, int y, Orientation orientation, int outX, int outY)
        {
            var rover = new Rover(x, y, orientation, _map);
            var roverDetector = new RoverDetector(rover, new SimpleDetector(new List<Tuple<int, int>> {}));
            var wasObstacle= roverDetector.MoveForward();

            Assert.AreEqual(outX, roverDetector.X);
            Assert.AreEqual(outY, roverDetector.Y);
            Assert.AreEqual(orientation, roverDetector.Orientation);
            Assert.AreEqual(false, wasObstacle);
        }

        [TestCase(Orientation.North, Orientation.West)]
        public void TurnLeft_CaseOrientation_ChangesOrientation(Orientation orientationBegin, Orientation orientationEnd)
        {
            var rover = new Rover(0, 0, orientationBegin, _map);
            var roverDetector = new RoverDetector(rover, new SimpleDetector(new List<Tuple<int, int>> {}));
            roverDetector.TurnLeft();

            Assert.AreEqual(0, roverDetector.X);
            Assert.AreEqual(0, roverDetector.Y);
            Assert.AreEqual(orientationEnd, roverDetector.Orientation);
        }

        [TestCase(Orientation.North, Orientation.East)]
        public void TurnRight_CaseOrientation_ChangesOrientation(Orientation orientationBegin, Orientation orientationEnd)
        {
            var rover = new Rover(0, 0, orientationBegin, _map);
            var roverDetector = new RoverDetector(rover, new SimpleDetector(new List<Tuple<int, int>> {}));
            roverDetector.TurnRight();

            Assert.AreEqual(0, roverDetector.X);
            Assert.AreEqual(0, roverDetector.Y);
            Assert.AreEqual(orientationEnd, roverDetector.Orientation);
        }

        #endregion -------- Test it behaves as before when no obstacles ---------

        [TestCase(0, 0, Orientation.East, 0, LengthY - 1)]
        [TestCase(3, 0, Orientation.East, 3, 9)]
        [TestCase(2, 0, Orientation.West, 2, LengthY - 1)]
        [TestCase(5, 0, Orientation.West, 5, 9)]
        [TestCase(1, 1, Orientation.South, 1, LengthY - 1)]
        [TestCase(3, 9, Orientation.North, 3, 9)]
        public void MoveForward_CasePositionXandYOrientationWithObstacles_ChangesPositionXorYAndObstacles(int x, int y, Orientation orientation, int outX, int outY)
        {
            var rover = new Rover(x, y, orientation, _map);
            var roverDetector = new RoverDetector(rover, _detector);
            var wasObstacle= roverDetector.MoveForward();

            Assert.AreEqual(outX, roverDetector.X);
            Assert.AreEqual(outY, roverDetector.Y);
            Assert.AreEqual(Orientation.North, roverDetector.Orientation);
            Assert.AreEqual(true, wasObstacle);
        }

        [TestCase(0, 0, Orientation.West, 0, LengthY - 1)]
        [TestCase(3, 0, Orientation.West, 3, 9)]
        [TestCase(2, 0, Orientation.East, 2, LengthY - 1)]
        [TestCase(5, 0, Orientation.East, 5, 9)]
        [TestCase(1, 1, Orientation.North, 1, LengthY - 1)]
        [TestCase(3, 9, Orientation.South, 3, 9)]
        public void MoveBackward_CasePositionXandYOrientationWithObstacles_ChangesPositionXorYAndObstacles(int x, int y, Orientation orientation, int outX, int outY)
        {
            var rover = new Rover(x, y, orientation, _map);
            var roverDetector = new RoverDetector(rover, _detector);
            var wasObstacle = roverDetector.MoveBackward();

            Assert.AreEqual(outX, roverDetector.X);
            Assert.AreEqual(outY, roverDetector.Y);
            Assert.AreEqual(Orientation.North, roverDetector.Orientation);
            Assert.AreEqual(true, wasObstacle);
        }
    }
}
