﻿using NUnit.Framework;

namespace MarsRover.Tests
{
    [TestFixture]
    public class RoverTest
    {
        private const int LengthX = 100;
        private const int LengthY = 50;

        [TestCase(0, 0, Orientation.West, LengthX - 1, 0)]
        [TestCase(LengthX - 1, 0, Orientation.West, LengthX - 2, 0)]
        [TestCase(0, 0, Orientation.East, 1, 0)]
        [TestCase(LengthX - 1, 0, Orientation.East, 0, 0)]
        [TestCase(0, 0, Orientation.North, 0, 1)]
        [TestCase(0, LengthY - 1, Orientation.North, 0, 0)]
        [TestCase(0, 0, Orientation.South, 0, LengthY - 1)]
        [TestCase(0, LengthY - 1, Orientation.South, 0, LengthY - 2)]
        public void MoveForward_CasePositionXandYOrientation_ChangesPositionXorYDependingOnOrientation(int x, int y, Orientation orientation, int outX, int outY)
        {
            var map = new SphereMap(LengthX, LengthY);
            var rover = new Rover(x, y, orientation, map);
            rover.MoveForward();

            Assert.AreEqual(outX, rover.X);
            Assert.AreEqual(outY, rover.Y);
            Assert.AreEqual(orientation, rover.Orientation);
        }

        [TestCase(LengthX - 1, 0, Orientation.West, LengthX - 2, 0)]
        [TestCase(0, 0, Orientation.West, LengthX - 1, 0)]
        [TestCase(LengthX - 1, 0, Orientation.East, 0, 0)]
        [TestCase(0, 0, Orientation.East, 1, 0)]
        [TestCase(0, LengthY - 1, Orientation.North, 0, 0)]
        [TestCase(0, 0, Orientation.North, 0, 1)]
        [TestCase(0, LengthY - 1, Orientation.South, 0, LengthY - 2)]
        [TestCase(0, 0, Orientation.South, 0, LengthY - 1)]
        public void MoveBackward_CasePositionXandYOrientation_ChangesPositionXorYDependingOnOrientation(int x, int y, Orientation orientation, int outX, int outY)
        {
            var map = new SphereMap(LengthX, LengthY);
            var rover = new Rover(x, y, orientation, map);
            rover.MoveForward();

            Assert.AreEqual(outX, rover.X);
            Assert.AreEqual(outY, rover.Y);
            Assert.AreEqual(orientation, rover.Orientation);
        }

        [TestCase(Orientation.North, Orientation.West)]
        [TestCase(Orientation.West, Orientation.South)]
        [TestCase(Orientation.South, Orientation.East)]
        [TestCase(Orientation.East, Orientation.North)]
        public void TurnLeft_CaseOrientation_ChangesOrientation(Orientation orientationBegin, Orientation orientationEnd)
        {
            var map = new SphereMap(LengthX, LengthY);
            var rover = new Rover(0, 0, orientationBegin, map);
            rover.TurnLeft();

            Assert.AreEqual(0, rover.X);
            Assert.AreEqual(0, rover.Y);
            Assert.AreEqual(orientationEnd, rover.Orientation);
        }

        [TestCase(Orientation.North, Orientation.East)]
        [TestCase(Orientation.West, Orientation.North)]
        [TestCase(Orientation.South, Orientation.West)]
        [TestCase(Orientation.East, Orientation.South)]
        public void TurnRight_CaseOrientation_ChangesOrientation(Orientation orientationBegin, Orientation orientationEnd)
        {
            var map = new SphereMap(LengthX, LengthY);
            var rover = new Rover(0, 0, orientationBegin, map);
            rover.TurnRight();

            Assert.AreEqual(0, rover.X);
            Assert.AreEqual(0, rover.Y);
            Assert.AreEqual(orientationEnd, rover.Orientation);
        }
    }
}
